import 'package:ventas_ejemplo/model/entity/user.dart';
import 'package:ventas_ejemplo/model/repositry/sale.dart';

import '../model/repositry/fb_auth.dart';
import '../model/repositry/user.dart';
import 'request/login.dart';
import 'request/register.dart';
import 'response/userinfo.dart';

class LoginController {
  late final UserRepository _userRepository;
  late final FirebaseAuthenticationRepository _authRepository;

  LoginController() {
    _userRepository = UserRepository();
    _authRepository = FirebaseAuthenticationRepository();
  }

  Future<UserInfoResponse> validateEmailPassword(LoginRequest request) async {
    await _authRepository.signInEmailPassword(request.email, request.password);

    //consultar el usuario que tenga el correo y la clave
    var user = await _userRepository.findByEmail(request.email);

    return UserInfoResponse(
      id: user.id,
      email: user.email,
      name: user.name,
      isAdmin: user.isAdmin,
    );
  }

  Future<void> registerNewUser(RegisterRequest request,
      {bool adminUser = false}) async {
    // Consulto si el usuario ya existe
    try {
      await _userRepository.findByEmail(request.email);
      return Future.error("Ya existe un usuario con este correo electronico");
    } catch (e) {
      // No exite el correo en la base de datos

      // Crear correo/clave en Firebase Authentication
      await _authRepository.createEmailPasswordAccount(
          request.email, request.password);

      // Agregar informacion adicional en base de datos
      _userRepository.save(UserEntity(
          email: request.email,
          name: request.name,
          address: request.address,
          phone: request.phone,
          isAdmin: adminUser));
    }
  }

  Future<void> logout() async {
    await _authRepository.signOut();
  }
}
