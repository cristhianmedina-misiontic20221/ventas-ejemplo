import 'package:ventas_ejemplo/model/entity/sale.dart';
import 'package:ventas_ejemplo/model/repositry/sale.dart';

class SaleController {
  late SaleRepository _repository;

  SaleController() {
    _repository = SaleRepository();
  }

  Future<void> save(SaleEntity sale) async {
    await _repository.newSale(sale);
  }

  Future<List<SaleEntity>> listAll(String id) async {
    return await _repository.getAllByUserId(id);
  }
}
