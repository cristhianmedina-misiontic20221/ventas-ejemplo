import 'package:cloud_firestore/cloud_firestore.dart';

class SaleEntity {
  late String? id;
  late String? user;
  late String? clientName;
  late String? address;
  late int? anmount;
  late int? parts;
  late String? periodicity;
  late double? value;

  SaleEntity(
      {this.user,
      this.clientName,
      this.address,
      this.anmount,
      this.parts,
      this.periodicity,
      this.value});

  factory SaleEntity.fromFirestore(
      DocumentSnapshot<Map<String, dynamic>> snapshot,
      SnapshotOptions? options) {
    var data = snapshot.data();

    return SaleEntity(
      user: data?["user"],
      clientName: data?["clientName"],
      address: data?["address"],
      anmount: data?["anmount"],
      parts: data?["parts"],
      periodicity: data?["periodicity"],
      value: data?["value"],
    );
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (user != null && user!.isNotEmpty) "user": user,
      if (clientName != null && clientName!.isNotEmpty)
        "clientName": clientName,
      if (address != null && address!.isNotEmpty) "address": address,
      if (anmount != null) "anmount": anmount,
      if (parts != null) "parts": parts,
      if (periodicity != null && periodicity!.isNotEmpty)
        "periodicity": periodicity,
      if (value != null) "value": value,
    };
  }
}
