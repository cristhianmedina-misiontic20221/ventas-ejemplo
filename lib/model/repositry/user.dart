import 'package:cloud_firestore/cloud_firestore.dart';
import '../entity/user.dart';

class UserRepository {
  late final CollectionReference _collection;
  UserRepository() {
    _collection = FirebaseFirestore.instance.collection("users");
  }

  Future<UserEntity> findByEmail(String email) async {
    final query = await _collection
        .where("email", isEqualTo: email)
        .withConverter<UserEntity>(
          fromFirestore: UserEntity.fromFirestore,
          toFirestore: (value, options) => value.toFirestore(),
        )
        .get();

    var users = query.docs.cast();

    if (users.isEmpty) {
      return Future.error("Ususrio no existe");
    }

    var user = users.first;

    var response = user.data();
    response.id = user.id;

    return response;

    // if (user == null) {
    //   throw Exception("Usuario no Existe");
    // }
  }

  Future<void> save(UserEntity user) async {
    //db.collection("users").doc(user.email).set(user.toFirestore());
    //await _collection.add(user.toFirestore());

    await _collection
        .withConverter(
          fromFirestore: UserEntity.fromFirestore,
          toFirestore: (value, options) => value.toFirestore(),
        )
        .add(user);
  }
}
