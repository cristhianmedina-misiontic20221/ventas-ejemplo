import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ventas_ejemplo/model/entity/sale.dart';

class SaleRepository {
  late final CollectionReference _collection;

  SaleRepository() {
    _collection = FirebaseFirestore.instance.collection("sales");
  }

  Future<void> newSale(SaleEntity sale) async {
    await _collection
        .withConverter<SaleEntity>(
            fromFirestore: SaleEntity.fromFirestore,
            toFirestore: (value, options) => value.toFirestore())
        .add(sale);
  }

  Future<List<SaleEntity>> getAllByUserId(String id) async {
    var query = await _collection
        .where("user", isEqualTo: id)
        .withConverter<SaleEntity>(
            fromFirestore: SaleEntity.fromFirestore,
            toFirestore: (value, _) => value.toFirestore())
        .get();

    var sales = query.docs.cast().map<SaleEntity>((e) {
      var sale = e.data();
      sale.id = e.id;
      return sale;
    });

    return sales.toList();
  }
}
