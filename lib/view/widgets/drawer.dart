import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ventas_ejemplo/controller/login.dart';
import 'package:ventas_ejemplo/view/pages/login.dart';
import '../pages/cash_close.dart';
import '../pages/payments.dart';

class DrawerWidget extends StatefulWidget {
  const DrawerWidget({super.key});

  @override
  State<DrawerWidget> createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  final _pref = SharedPreferences.getInstance();
  final _loginController = LoginController();
  String _name = "";
  String _email = "";
  bool _isAdmin = false;

  @override
  void initState() {
    super.initState();

    _pref.then((pref) {
      setState(() {
        _name = pref.getString("name") ?? "N/A";
        _email = pref.getString("email") ?? "N/A";
        _isAdmin = pref.getBool("admin") ?? false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: const BoxDecoration(
              color: Colors.blue,
            ),
            child: _header(),
          ),
          ListTile(
            leading: const Icon(Icons.payment),
            title: const Text('Cobros'),
            onTap: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => const PaymentsPage(),
                ),
              );
            },
          ),
          ListTile(
            leading: const Icon(Icons.shopping_bag),
            title: const Text('Ventas'),
            onTap: () {},
          ),
          ListTile(
            leading: const Icon(Icons.supervisor_account),
            title: const Text('Clientes'),
            onTap: () {},
          ),
          ListTile(
            leading: const Icon(Icons.money),
            title: const Text('Cierrre de caja'),
            onTap: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => const CashClosePage()),
              );
            },
          ),
          if (_isAdmin)
            ListTile(
              leading: const Icon(Icons.mediation_rounded),
              title: const Text('Gestion de productos'),
              onTap: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const CashClosePage()),
                );
              },
            ),
          ListTile(
            leading: const Icon(Icons.exit_to_app),
            title: const Text('Cerrar Cesion'),
            onTap: () async {
              var nav = Navigator.of(context);

              // Cerrar sesion en Auth de Firebase
              _loginController.logout();

              // Limpiar las preferences
              var pref = await _pref;
              pref.remove("uid");
              pref.remove("email");
              pref.remove("name");
              pref.remove("admin");

              // Volver a pagina de login
              nav.pushReplacement(MaterialPageRoute(
                builder: (context) => LoginPage(),
              ));
            },
          ),
        ],
      ),
    );
  }

  Widget _header() {
    //Consultar los datos de la cabecera
    const image = Icon(Icons.manage_accounts);

    return Row(
      children: [
        const CircleAvatar(
          radius: 30,
          child: image,
        ),
        const SizedBox(width: 8),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                _name,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                ),
              ),
              const SizedBox(height: 8),
              Text(
                _email,
                style: const TextStyle(
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
