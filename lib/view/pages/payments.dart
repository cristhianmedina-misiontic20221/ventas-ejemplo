import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ventas_ejemplo/controller/sale.dart';
import 'package:ventas_ejemplo/model/entity/sale.dart';
import 'package:ventas_ejemplo/view/pages/new_sale.dart';
import '../widgets/drawer.dart';

class PaymentsPage extends StatefulWidget {
  const PaymentsPage({super.key});

  @override
  State<PaymentsPage> createState() => _PaymentsPageState();
}

class _PaymentsPageState extends State<PaymentsPage> {
  List<SaleEntity> _lista = [];
  final _pref = SharedPreferences.getInstance();
  final _saleController = SaleController();

  @override
  void initState() {
    super.initState();
    _listarCobros();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Ventas"),
        ),
        drawer: const DrawerWidget(),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "Cobros",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: _lista.length,
                  itemBuilder: (context, index) => ListTile(
                    leading: const CircleAvatar(),
                    title: Text(_lista[index].clientName!),
                    subtitle: Text(_lista[index].address!),
                    trailing: IconButton(
                      icon: const Icon(Icons.phone),
                      onPressed: () {
                        // TODO Realizar la llamada Telefonica
                      },
                    ),
                    onTap: () {
                      // TODO: Ir a la ventana detalle del cobro
                    },
                  ),
                ),
              )
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add_shopping_cart),
          onPressed: () async {
            await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => NewSalePage(),
              ),
            );

            if (!mounted) return;

            _listarCobros();
          },
        ),
      ),
    );
  }

  void _listarCobros() {
    _pref.then((pref) {
      var id = pref.getString("uid") ?? "";
      _saleController.listAll(id).then((value) {
        setState(() {
          _lista = value;
        });
      });
    });
  }
}
