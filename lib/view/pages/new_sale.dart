import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../controller/sale.dart';
import '../../model/entity/sale.dart';

class NewSalePage extends StatelessWidget {
  final _pref = SharedPreferences.getInstance();
  late final SaleEntity _sale;
  late final SaleController _controller;

  NewSalePage({super.key}) {
    _sale = SaleEntity();
    _controller = SaleController();
    _pref.then(
      (pref) {
        _sale.user = pref.getString("uid");
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ventas"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "Nueva venta",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              _formulario(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _formulario(context) {
    final formkey = GlobalKey<FormState>();

    return Form(
      key: formkey,
      child: Column(
        children: [
          _campoCliente(),
          const SizedBox(height: 8),
          _campoDireccion(),
          const SizedBox(height: 8),
          _campoMonto(),
          const SizedBox(height: 8),
          Row(
            children: [
              _numeroCuotas(),
              const SizedBox(width: 8),
              _periodicidad(),
            ],
          ),
          const SizedBox(height: 8),
          _valorCuota(),
          const SizedBox(height: 8),
          ElevatedButton(
            child: const Text("Guardar"),
            onPressed: () async {
              if (formkey.currentState!.validate()) {
                formkey.currentState!.save();
                // Guardar los datos en la BD
                try {
                  await _controller.save(_sale);

                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text("Venta Realizada con exito"),
                    ),
                  );

                  //Volver a la pantalla antrior
                  Navigator.pop(context);
                } catch (e) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text("error: $e"),
                    ),
                  );
                }
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _campoCliente() {
    return TextFormField(
      autofocus: true,
      keyboardType: TextInputType.name,
      decoration: const InputDecoration(
        icon: Icon(Icons.manage_accounts),
        border: OutlineInputBorder(),
        labelText: 'Cliente',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El campo es obligatorio";
        }
        return null;
      },
      onSaved: (value) {
        _sale.clientName = value!;
      },
    );
  }

  Widget _campoDireccion() {
    return TextFormField(
      keyboardType: TextInputType.streetAddress,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Direccion',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El campo es obligatorio";
        }
        return null;
      },
      onSaved: (value) {
        _sale.address = value!;
      },
    );
  }

  Widget _campoMonto() {
    return TextFormField(
      initialValue: "0",
      textAlign: TextAlign.right,
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Monto',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El campo es obligatorio";
        }
        if (int.parse(value) < 1) {
          return "El valor no es valido";
        }
        return null;
      },
      onSaved: (value) {
        _sale.anmount = int.tryParse(value!);
      },
    );
  }

  Widget _numeroCuotas() {
    return Expanded(
      child: TextFormField(
        initialValue: "0",
        textAlign: TextAlign.right,
        keyboardType: TextInputType.number,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Numero de cuotas',
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "El campo es obligatorio";
          }
          if (int.parse(value) < 1) {
            return "El valor no es valido";
          }
          return null;
        },
        onSaved: (value) {
          _sale.parts = int.tryParse(value!);
        },
      ),
    );
  }

  Widget _periodicidad() {
    var opciones = <String>["Diario", "Semanal", "Quincenal", "Mensual"];
    var valor = opciones[1];

    return Expanded(
      child: DropdownButtonFormField(
        value: valor,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Periodicidad',
        ),
        items: opciones
            .map<DropdownMenuItem<String>>(
                (String value) => DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    ))
            .toList(),
        onChanged: (value) {
          _sale.periodicity = value;
        },
      ),
    );
  }

  Widget _valorCuota() {
    return TextFormField(
      enabled: false,
      textAlign: TextAlign.right,
      initialValue: "0",
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Valor Cuota',
      ),
      onSaved: (value) {
        _sale.value = double.tryParse(value!);
      },
    );
  }
}
