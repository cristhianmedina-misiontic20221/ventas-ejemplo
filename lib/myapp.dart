import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ventas_ejemplo/view/pages/payments.dart';
import 'view/pages/login.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _pref = SharedPreferences.getInstance();

  Widget _init = const Scaffold(
    body: Center(
      child: CircularProgressIndicator(),
    ),
  );

  //Widget _init = LoginPage();

  @override
  void initState() {
    super.initState();
    _pref.then((pref) {
      setState(() {
        if (pref.getString("uid") != null) {
          _init = const PaymentsPage();
        } else {
          _init = LoginPage();
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Ventas a domicilio",
      home: _init,
    );
  }
}
